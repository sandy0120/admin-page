import request from '@/utils/request'

export function loginApi(data) {
  return request({
    url: '/api/v1/user/login',
    method: 'post',
    data
  })
}

export function addUserApi(data) {
  return request({
    url: '/api/v1/user',
    method: 'post',
    data
  })
}

export function editUserApi(data) {
  return request({
    url: '/api/v1/user',
    method: 'put',
    data
  })
}

export function getInfo(data) {
  return request({
    url: `/api/v1/user/${data}`,
    method: 'get'
  })
}

export function getAllInfo() {
  return request({
    url: '/api/v1/user',
    method: 'get'
  })
}

export function getLoginList(data) {
  return request({
    url: '/api/v1/user/user-login',
    method: 'post',
    data
  })
}

export function deleteApi(data) {
  return request({
    url: '/api/v1/user',
    method: 'delete',
    data
  })
}

// export function logout() {
//   return request({
//     url: '/vue-admin-template/user/logout',
//     method: 'post'
//   })
// }
